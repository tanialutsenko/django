from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseNotFound, Http404

from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from django.template.loader import render_to_string
from django.template.defaultfilters import slugify
menu = ['о сайте', 'Добавить статью', 'Обратная связь', 'Войти']

class MyClass:
    def __init__(self,a,b):
        self.a = a
        self.b = b


def index(request):
    # t = render_to_string('women/index.html')
    # return HttpResponse(t)
    data = {'title': 'главная страница?',
            'main_title': 'title',
            'menu': menu,
            'float': 28.78,
            'list': [ 1, 2, 'abc',True],
            'set':{1,2,3,2,5},
            'dict':{ 'key_1':'value_1','key_2':'value_2'},
            'obj': MyClass(10,20),
            'url': slugify(" The main page")

    }
    return render(request, 'women/index.html', context=data)

def about(request):
    return render(request, 'women/about.html', {'title': 'О сайте'})

def categories(request,cat_id):
    return HttpResponse(f"<h1>Страницы по категориям</h1><p>id:{cat_id}<p>")  #http://127.0.0.1:8000/cats/1/

def categories_by_slug(request,cat_slug):
    return HttpResponse(f"<h1>Страницы по категориям</h1><p>slug:{cat_slug}<p>")  #http://127.0.0.1:8000/cats/1/

def archive(request,year):
    if year > 2023:
        # raise Http404()    #обработка ошибки 404
        # return redirect("home")   #перенаправление на страницу home
        uri = reverse('cats', args=('music',))
        # return redirect(uri)
        return HttpResponseRedirect('/')
    return HttpResponse(f"<h1>Архив по годам</h1><p>{year}<p>")  #http://127.0.0.1:8000/cats/1/

def page_not_found(request,exception):
    return HttpResponseNotFound(f"<h1>Страница не найдена</h1>")  #http://127.0.0.1:8000/cats/1/